from matplotlib import pyplot as plt

def ploter(img, imgName, figName, inter = None, cmap = None, svDir = None):  # ploter([...],[...],"...")
    if len(img) < 1 or len(imgName) != len(img):    # images and image legends lists should be the same length and greater than 0
        return

    columns = len(img) // 2 if len(img) // 2 > 1 else len(img)
    rows = len(img) // columns
    fig = plt.figure(num=figName, figsize=(columns * 5, rows * 5))
    i = 0
    while i < len(img):
        fig.add_subplot(rows, columns, i + 1)
        plt.imshow(img[i], interpolation = inter, cmap = cmap)
        plt.title(imgName[i])
        i += 1

    if svDir is not None:
        plt.savefig(svDir + figName.replace(' ', '') + '.png')

    plt.show()