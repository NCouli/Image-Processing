# versions
# python:                   3.6
# skimage:                  0.19.3
# opencv-python:		    3.4.2.16
# opencv-contrib-python:	3.4.2.16
# numpy:                    1.19.5
# matplotlib:               3.6.2

import cv2 as cv
from numpy import uint8
from skimage.transform import resize
import comparisonFuncs as cmpFunc
import ploter as pl

iniImag = cv.imread("../Assignment/cameraman.tif")
res = []    # upsampled images
for i in [True, False]:
    for j in [[1/2, 1/4], [1/4, 1/2], [1/8, 1/8]]:
        title = "Anti-aliasing= " + str(i) + " - " + "Rows= " + str(j[0]) + " - Columns= " + str(j[1])
        print(title)
        for k in [0, 1, 3]: #in skimage.transform.resize -> orderr: 0: nearest neighbour, 1: bilinear, 3: bicubic
            dim = (int(iniImag.shape[0] * j[0]), int(iniImag.shape[1] * j[1]))
            rsd = resize(iniImag, dim, preserve_range = True, anti_aliasing = i, order = k)   # Downsampling initial image

            dim = (int(rsd.shape[0] * (j[0] ** -1)), int(rsd.shape[1] * (j[1] ** -1)))      # (1 / x) ^ -1 = x | e.g. (1 / 4) ^ -1 = 4
            rsd = resize(rsd, dim, preserve_range = True, anti_aliasing = i, order = k).astype(uint8) # Upsampling to initial image dimentions
            res.append(rsd)
        # showcase MSE and PSNR (with 4 decimals)
        print("Nearest Neighbor: MSE= ", f'{cmpFunc.MSE(iniImag, res[len(res) - 3]):.4f}', " PSNR= ", f'{cmpFunc.PSNR(iniImag, res[len(res) - 3]):.4f}')
        print("Bi-linear       : MSE= ", f'{cmpFunc.MSE(iniImag, res[len(res) - 2]):.4f}', " PSNR= ", f'{cmpFunc.PSNR(iniImag, res[len(res) - 2]):.4f}')
        print("Bi-cubic        : MSE= ", f'{cmpFunc.MSE(iniImag, res[len(res) - 1]):.4f}', " PSNR= ", f'{cmpFunc.PSNR(iniImag, res[len(res) - 1]):.4f}')
        # plot all images in one figure with the same sampling numbers and different interpolation methods
        pl.ploter([iniImag] + res[len(res) - 3 : len(res)], ["Original", "Nearest", "Linear", "Cubic"], title)