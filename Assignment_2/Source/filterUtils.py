import cv2 as cv
import numpy as np

def normalization(x, mu, sd):
    return 1 / (np.sqrt(2 * np.pi) * sd) * np.e ** (-np.power((x - mu) / sd, 2) / 2)

def gaussian_kernel(size = 1, sigma = 1):
    kernel_1D = np.linspace(-(size // 2), size // 2, size)
    for i in range(size):
        kernel_1D[i] = normalization(kernel_1D[i], 0, sigma)

    kernel_2D = np.outer(kernel_1D.T, kernel_1D.T)
    kernel_2D /= kernel_2D.max()
    return kernel_2D

def zPadding(image, num_rows = 1, num_cols = 1):
    pad_height = (num_rows - 1) // 2
    pad_width = (num_cols - 1) // 2
    padded_image = np.zeros((image.shape[0] + (2 * pad_height), image.shape[1] + (2 * pad_width)))
    padded_image[pad_height : padded_image.shape[0] - pad_height, pad_width : padded_image.shape[1] - pad_width] = image
    return padded_image

def grayscaleConv(image):
    if len(image.shape) == 3:
        print("Found 3 Channels : {}".format(image.shape))
        image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
        print("Converted to Gray Channel. Size : {}".format(image.shape))
        return image

    return image