import numpy as np
from scipy import fftpack
import filterUtils as fltFuncs

def convolution(image, kernel):
    kernel = np.flipud(np.fliplr(kernel))
    image_row, image_col = image.shape
    kernel_row, kernel_col = kernel.shape
    output = np.zeros(image.shape)
    padded_image = fltFuncs.zPadding(image, kernel_row, kernel_col)

    for row in range(image_row):
        for col in range(image_col):
            output[row, col] = np.sum(kernel * padded_image[row : row + kernel_row, col : col + kernel_col])

    return output

def fftConvolution(image, kernel):
    rr = image.shape[0] + kernel.shape[0] - 1
    cc = image.shape[1] + kernel.shape[1] - 1
    ifftIm = fftpack.ifft2(fftpack.fft2(image, (rr, cc)) * (fftpack.fft2(kernel, (rr, cc))))

    padIfftImR = kernel.shape[0] // 2
    padIfftImC = kernel.shape[1] // 2
    return ifftIm[padIfftImR : image.shape[0] + padIfftImR , padIfftImC : image.shape[1] + padIfftImC].real