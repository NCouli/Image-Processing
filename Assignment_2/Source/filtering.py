# versions
# python:                   3.6
# skimage:                  0.19.3
# opencv-python:            3.4.2.16
# opencv-contrib-python:    3.4.2.16
# numpy:                    1.19.5
# matplotlib:               3.6.2
# scipy:                    1.5.4

import sys
import time
import cv2 as cv
import scipy.signal
import convolutions as cn
import filterUtils as fltFuncs

if "../../Assignment_1/Source/" not in sys.path:
    sys.path.append("../../Assignment_1/Source/")
    import ploter as pl
    import comparisonFuncs as cmpFunc

iniImag = cv.imread("../Assignment/lena_gray_512.tif", cv.IMREAD_GRAYSCALE)
iniImag = fltFuncs.grayscaleConv(iniImag)
print("Image Shape : {}".format(iniImag.shape))
kernel = fltFuncs.gaussian_kernel(15, sigma = 20)
print("Kernel Shape : {}".format(kernel.shape))

# Task 1 custom convolution func
print("------------------------------Task 1------------------------------")
st = time.time()
conv = cn.convolution(iniImag, kernel)
tt = time.time()
print("Output Image size : {}".format(conv.shape))
print("Calculating MSE with convolved image", "{:.4f}".format(cmpFunc.MSE(iniImag, conv)))
print("Calculating PSNR with convolved image", "{:.4f}".format(cmpFunc.PSNR(iniImag, conv)))
print("Custom convolution function run for: ", "{:.6f}".format(tt - st), "seconds")

# Task 2 using conv2d (identical in python (but not exactly the same) is scipy.signal.convolve2d)
print("------------------------------Task 2------------------------------")
st = time.time()
conv2d = scipy.signal.convolve2d(iniImag, kernel, mode = 'same')
tt = time.time()
print("Calculating MSE of custom function and conv2d: ", "{:.4f}".format(cmpFunc.MSE(conv, conv2d)))
print("convolve2d(conv2d) function run for: ", "{:.6f}".format(tt - st), "seconds")

# Task 3 using imfilter (identical in python (but not exactly the same) is scipy.signal.convolve)
print("------------------------------Task 3------------------------------")
st = time.time()
imfilter = scipy.signal.convolve(iniImag, kernel, mode = 'same')
tt = time.time()
print("Calculating MSE of custom function and imfilter: ", "{:.4f}".format(cmpFunc.MSE(conv, imfilter)))
print("convolve(imfilter) function run for: ", "{:.6f}".format(tt - st), "seconds")
# Task 4 custom convolution func using fourier transform
print("------------------------------Task 4------------------------------")
st = time.time()
fftConvolution = cn.fftConvolution(iniImag, kernel)
tt = time.time()
print("Calculating MSE with custom function which uses Fourier transform and a convolved image: ", "{:.4f}".format(cmpFunc.MSE(conv, fftConvolution)))
print("Custom convolution function using Fourier transform run for: ", "{:.6f}".format(tt - st), "seconds")

svDir = ''
svDir = '../Results/'
if not svDir == "":
    pl.ploter([kernel], ["Kernel ( {}X{} )".format(kernel.shape[0], kernel.shape[1])], "gausKernel{}x{}".format(kernel.shape[0], kernel.shape[1]), inter = 'none', cmap = 'gray', svDir = svDir)
    pl.ploter([fltFuncs.zPadding(iniImag, kernel.shape[0], kernel.shape[1])], ["Padded Image"], "paddedImage{}x{}".format(kernel.shape[0], kernel.shape[1]), cmap = 'gray', svDir = svDir)
    pl.ploter([conv], ["Convolved Image"], "convolvedGaus{}x{}".format(kernel.shape[0], kernel.shape[1]), cmap = 'gray', svDir = svDir)
    pl.ploter([fftConvolution], ["Convolved w/ Fourier"], "convolvedGaus{}x{}fft".format(kernel.shape[0], kernel.shape[1]), cmap = 'gray', svDir = svDir)
