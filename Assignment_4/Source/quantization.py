# versions
# python:                   3.6
# skimage:                  0.19.3
# opencv-python:            3.4.2.16
# opencv-contrib-python:    3.4.2.16
# numpy:                    1.19.5
# matplotlib:               3.6.2
# scipy:                    1.5.4

import sys
import cv2 as cv

import quantizerUtils as qn
import haarTrans as haar

if "../../Assignment_1/Source/" not in sys.path:
    sys.path.append("../../Assignment_1/Source/")

import ploter as pl
import comparisonFuncs as cmpFunc

if "../../Assignment_2/Source/" not in sys.path:
    sys.path.append("../../Assignment_2/Source/")

import filterUtils as fltFuncs

def stepA(image, signal):
    mseData = []
    for num_lvl in range(9):
        step_size = qn.lvlCalc(signal = signal, num_lvl = num_lvl)
        qn.charFuncPlotter(signalRange = signal, interval = step_size, num_lvl = num_lvl, svDir = "../Results/characteristicFuncs/")

        # quantize the image using 8 levels
        quantized_image = qn.uni_scalar(image, step_size)
        # display the original and quantized images side by side
        pl.ploter([image, quantized_image], ["Original Image", "Quantized Image"], "Quantization {} levels".format(num_lvl), cmap = 'gray', svDir = "../Results/quantizedImages/")
        mseData.append(cmpFunc.MSE(image, quantized_image))
        print("MSE of r:{}= ".format(num_lvl),f'{mseData[num_lvl]:0.5}')

    qn.distorPloter(mseData, svDir = "../Results/rateDistortion/")


def stepB(image, signal):
    # Level 1 of Haar Transform
    LL1, (LH1, HL1, HH1) = haar.haarFilter(image)
    pl.ploter([LL1, HL1, LH1, HH1], ["LL1", "HL1", "LH1", "HH1"], "Haar Lvl 1", cmap = 'gray', svDir = "../Results/haarAnalysis/lvl1/")
    pl.ploter([haar.haarImagPlot(LL1, HL1, LH1, HH1)], ["Haar Level 1"], "Haar Lvl 1 concat", cmap = 'gray', svDir = "../Results/haarAnalysis/stackup/")

    # Level 2 of Haar Transform
    LL2, (LH2, HL2, HH2) = haar.haarFilter(LL1)
    pl.ploter([LL2, HL2, LH2, HH2], ["LL2", "HL2", "LH2", "HH2"], "Haar Lvl 2", cmap = 'gray', svDir = "../Results/haarAnalysis/lvl2/")

    c = haar.haarImagPlot(LL2, HL2, LH2, HH2)
    pl.ploter([c, HL1, LH1, HH1], ["LL1 - New Haar Level", "HL1", "LH1", "HH1"], "Haar Lvl 2 concat", cmap = 'gray', svDir = "../Results/haarAnalysis/stackup/")
    c = haar.haarImagPlot(c, HL1, LH1, HH1)
    pl.ploter([c], ["2 Levels Haar Analysis"], "Haar Lvl 1-2 concat", cmap = 'gray', svDir = "../Results/haarAnalysis/stackup/")

    svDir = "../Results/haarAnalysis/lvl1/quantized/"
    step_size = qn.lvlCalc(signal = signal, num_lvl = 2)
    qLL1, qHL1, qLH1, qHH1 = qn.quantizeHaarLvl([LL1, LH1, HL1, HH1], step_size)
    pl.ploter([LL1, qLL1], ["Original Image", "Quantized Image"], "LL1_HaarLvl{}Quantization {} levels".format(1, 2), cmap = 'gray', svDir = svDir)
    pl.ploter([LH1, qLH1], ["Original Image", "Quantized Image"], "LH1_HaarLvl{}Quantization {} levels".format(1, 2), cmap = 'gray', svDir = svDir)
    pl.ploter([HL1, qHL1], ["Original Image", "Quantized Image"], "HL1_HaarLvl{}Quantization {} levels".format(1, 2), cmap = 'gray', svDir = svDir)
    pl.ploter([HH1, qHH1], ["Original Image", "Quantized Image"], "HH1_HaarLvl{}Quantization {} levels".format(1, 2), cmap = 'gray', svDir = svDir)

    svDir = "../Results/haarAnalysis/lvl2/quantized/"
    step_size = qn.lvlCalc(signal = signal, num_lvl = 4)
    qLL2, qHL2, qLH2, qHH2 = qn.quantizeHaarLvl([LL2, LH2, HL2, HH2], step_size)
    pl.ploter([LL2, qLL2], ["Original Image", "Quantized Image"], "LL2_HaarLvl{}Quantization {} levels".format(2, 4), cmap = 'gray', svDir = svDir)
    pl.ploter([LH2, qLH2], ["Original Image", "Quantized Image"], "LH2_HaarLvl{}Quantization {} levels".format(2, 4), cmap = 'gray', svDir = svDir)
    pl.ploter([HL2, qHL2], ["Original Image", "Quantized Image"], "HL2_HaarLvl{}Quantization {} levels".format(2, 4), cmap = 'gray', svDir = svDir)
    pl.ploter([HH2, qHH2], ["Original Image", "Quantized Image"], "HH2_HaarLvl{}Quantization {} levels".format(2, 4), cmap = 'gray', svDir = svDir)

    LL_entropy1 = haar.calculate_entropy(qLL1)
    LH_entropy1 = haar.calculate_entropy(qLH1)
    HL_entropy1 = haar.calculate_entropy(qHL1)
    HH_entropy1 = haar.calculate_entropy(qHH1)

    LL_entropy2 = haar.calculate_entropy(qLL2)
    LH_entropy2 = haar.calculate_entropy(qLH2)
    HL_entropy2 = haar.calculate_entropy(qHL2)
    HH_entropy2 = haar.calculate_entropy(qHH2)

    total_entropy = (LL_entropy2 * qLL2.size + LH_entropy2 * qLH2.size + HL_entropy2 * qHL2.size + HH_entropy2 * qHH2.size +
                    LH_entropy1 * qLH1.size + HL_entropy1 * qHL1.size + HH_entropy1 * qHH1.size) / image.size

    print("Level 1 Haar entropy")
    print("LL entropy: ", f'{LL_entropy1:.5f}', "\nLH entropy: ", f'{LH_entropy1:.5f}',
          "\nHL entropy: ", f'{HL_entropy1:.5f}', "\nHH entropy: ", f'{HH_entropy1:.5f}')
    print("Level 2 Haar entropy")
    print("LL entropy: ", f'{LL_entropy2:.5f}', "\nLH entropy: ", f'{LH_entropy2:.5f}',
          "\nHL entropy: ", f'{HL_entropy2:.5f}', "\nHH entropy: ", f'{HH_entropy2:.5f}')

    print("Total entropy=", f'{total_entropy:0.5}')

    recImage = haar.inverseHaar(haar.inverseHaar(qLL2, qLH2, qHL2, qHH2), qLH1, qHL1, qHH1)
    pl.ploter([image, recImage], ["Origina Image", "Reconstructed Image"], "Reconstructed Haar quantized Tranformation", cmap='gray', svDir = "../Results/haarAnalysis/reconstructed/")
    print("PSNR of reconstructed image= ", f'{cmpFunc.PSNR(image, recImage):.5f}')


# load a sample image
image = cv.imread("../../Assignment_1/Assignment/cameraman.tif", cv.IMREAD_GRAYSCALE)
image = fltFuncs.grayscaleConv(image)

signal = [-255, 255]
stepA(image, signal)
stepB(image, signal)