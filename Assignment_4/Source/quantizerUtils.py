import numpy as np
import matplotlib.pyplot as plt

def uni_scalar(image, step_size):

    # Quantize the image
    quantized_img = np.floor(np.abs(image) / step_size + 1 / 2)
    # Apply the sign function and step size
    quantized_img = step_size * np.sign(quantized_img) * quantized_img

    return quantized_img

def lvlCalc(signal, num_lvl = 0):

    levels = 2 ** num_lvl
    return (signal[1] - signal[0]) / levels

# Define the characteristic function of the quantizer with sign
def char_fn(x, levels, interval):

    index = np.digitize(x, levels) - 1
    return (levels[index] + interval / 2)

def charFuncPlotter(signalRange, interval, num_lvl, svDir = None):
    # Define the quantization levels and interval
    levels = np.linspace(signalRange[0], signalRange[1], 2 ** num_lvl)

    # Plot the characteristic function
    x = np.linspace(signalRange[0], signalRange[1], 1000)
    fig, ax = plt.subplots()
    ax.plot(x, char_fn(x, levels, interval))
    ax.set_xlabel('Input signal')
    ax.set_ylabel('Output signal')
    ax.set_title('Characteristic function(r={})'.format(num_lvl))

    if svDir is not None:
        plt.savefig(svDir + "charactFunc-precision={}".format(num_lvl).replace(' ', '') + '.png')

    plt.show()

def distorPloter(signalRange, svDir = None):
    # Plot the characteristic function
    x = np.linspace(signalRange[0], signalRange[1], 1000)
    fig, ax = plt.subplots()
    ax.plot(signalRange, 'b--o')
    ax.set_xlabel('R')
    ax.set_ylabel('MSE Error')
    ax.set_title('Rate-Distortion Curve')

    if svDir is not None:
        plt.savefig(svDir + "rateDistortion Curve".replace(' ', '') + '.png')
    plt.show()

def quantizeHaarLvl(images, step_size):

    qLL = uni_scalar(images[0], step_size)
    qHL = uni_scalar(images[1], step_size)
    qLH = uni_scalar(images[2], step_size)
    qHH = uni_scalar(images[3], step_size)
    return qLL, qHL, qLH, qHH