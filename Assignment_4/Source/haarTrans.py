import pywt
import numpy as np

def haarFilter(image):
    # Perform the Haar transformation
    return pywt.dwt2(image, 'haar') # returns LL, (LH, HL, HH)

def inverseHaar(TL, TR, BL, BR):
    # Perform the inverse Haar transformation
    return pywt.idwt2((TL, (TR, BL, BR)), 'haar')

def haarImagPlot(TL, TR, BL, BR):

    a = np.concatenate((TL, TR), axis=1)
    b = np.concatenate((BL, BR), axis=1)
    c = np.concatenate((a, b), axis=0)
    return c

def calculate_entropy(data):
    _, counts = np.unique(data, return_counts=True)
    probs = counts / np.sum(counts)
    entropy = - np.sum(probs * np.log2(probs))
    return entropy