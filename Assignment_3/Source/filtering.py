# versions
# python:                   3.6
# skimage:                  0.19.3
# opencv-python:            3.4.2.16
# opencv-contrib-python:    3.4.2.16
# numpy:                    1.19.5
# matplotlib:               3.6.2
# scipy:                    1.5.4

import sys
import cv2 as cv
import numpy as np
import morphOperations as mrpOp
from scipy.ndimage import binary_fill_holes
from skimage.morphology import skeletonize, thin

if "../../Assignment_1/Source/" not in sys.path:
    sys.path.append("../../Assignment_1/Source/")

import ploter as pl

if "../../Assignment_2/Source/" not in sys.path:
    sys.path.append("../../Assignment_2/Source/")

import filterUtils as fltFuncs

axones1 = cv.imread("../Assignment/axones1.png", cv.IMREAD_GRAYSCALE)
axones2 = cv.imread("../Assignment/axones2.png", cv.IMREAD_GRAYSCALE)

axones1 = fltFuncs.grayscaleConv(axones1)
axones2 = fltFuncs.grayscaleConv(axones2)

opening1_1 = mrpOp.operation(axones1, "opening", "ellipse", 4, 4)
closing1_1 = mrpOp.operation(opening1_1, "closing", "ellipse", 5, 5)

erosion1_1 = mrpOp.operation(closing1_1, "erosion", "ellipse", 2, 2, 2)

dilaton1_1 = mrpOp.operation(erosion1_1, "dilation", "ellipse", 2, 2)

gradient1_1 = mrpOp.operation(dilaton1_1, "gradient", "ellipse", 5, 5)
dilaton1_2 = mrpOp.operation(gradient1_1, "dilation", "cross", 3, 3)

ret2, th2 = cv.threshold(dilaton1_2, 100, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)

skel = skeletonize(gradient1_1 > ret2)
filled_image = binary_fill_holes(skel)

dilaton1_3 = mrpOp.operation(filled_image.astype(np.uint8), "dilation", "ellipse", 18, 18)
erosion1_2 = mrpOp.operation(dilaton1_3, "erosion", "ellipse", 5, 5)

thinned_image = thin(erosion1_2, max_iter = np.inf)

morphOper = ["Opening, Ellipse-4x4", "Closing, Ellipse-5x5", "Erosion, Ellipse-2x2, iter=2",
             "Dilation, Ellipse-2x2, iter=1", "Gradient, Ellipse-5x5", "Dilation, Cross-3x3, iter=1",
             "Binarization - Otsu's method", "Skeletalization", "Fill holes",
             "Dilation, Ellipse-18x18, iter=1", "Erosion, Ellipse-5x5, iter=1", "Thinning"]

opOut = [opening1_1, closing1_1, erosion1_1, dilaton1_1, gradient1_1, dilaton1_2,
         th2, skel, filled_image, dilaton1_3, erosion1_2, thinned_image]

for i in range(len(opOut)):
    pl.ploter([opOut[i]], [morphOper[i]], "Stage " + str(i + 1), cmap = 'gray', svDir = "../Results/axones1/")

pl.ploter([axones1, thinned_image], ["Original Image", "Final Image"], "Final Result", cmap = 'gray', svDir = "../Results/axones1/")

#---------------------------------------------------------------------------------------------------------------------------------------------
opening2_1 = mrpOp.operation(axones2, "opening", "cross", 6, 6)
closing2_1 = mrpOp.operation(opening2_1, "closing", "ellipse", 3, 3)
erosion2_1 = mrpOp.operation(closing2_1, "erosion", "ellipse", 3, 3)

gradient2_1 = mrpOp.operation(erosion2_1, "gradient", "cross", 3, 3)

erosion2_2 = mrpOp.operation(gradient2_1, "erosion", "cross", 3, 3)

ret2, th2 = cv.threshold(erosion2_2, 22, 255, cv.THRESH_BINARY)
skel = skeletonize(th2 > ret2)
filled_image = binary_fill_holes(skel)

dilaton2_1 = mrpOp.operation(filled_image.astype(np.uint8), "dilation", "ellipse", 50, 50)

thinned_image = thin(dilaton2_1, max_iter = np.inf)

morphOper = ["Opening, Cross-6x6", "Closing, Ellipse-3x3", "Erosion, Ellipse-3x3", "Gradient, Cross-3x3", "Erosion, Cross-3x3, iter=1",
             "Binarization - 22:255", "Skeletalization", "Fill holes",
             "Dilation, Ellipse-50x50, iter=1", "Thinning"]

opOut = [opening2_1, closing2_1, erosion2_1, gradient2_1, erosion2_2, th2,
         skel, filled_image, dilaton2_1, thinned_image]

for i in range(len(opOut)):
    pl.ploter([opOut[i]], [morphOper[i]], "Stage " + str(i + 1), cmap = 'gray', svDir = "../Results/axones2/")

pl.ploter([axones2, thinned_image], ["Original Image", "Final Image"], "Final Result", cmap = 'gray', svDir = "../Results/axones2/")

