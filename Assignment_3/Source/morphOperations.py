import cv2 as cv

def operation(image, operation, shape, dimR, dimC, iter = 1):
    if shape.lower() == "rectangle":
        shape = cv.MORPH_RECT
    elif shape.lower() == "ellipse":
        shape = cv.MORPH_ELLIPSE
    elif shape.lower() == "cross":
        shape = cv.MORPH_CROSS

    kernel = cv.getStructuringElement(shape, (dimR, dimC))

    if operation.lower() == "erosion":
        image = cv.erode(image, kernel, iterations = iter)
    elif operation.lower() == "dilation":
        image = cv.dilate(image, kernel, iterations = iter)
    elif operation.lower() == "opening":
        image = cv.morphologyEx(image, cv.MORPH_OPEN, kernel)
    elif operation.lower() == "closing":
        image = cv.morphologyEx(image, cv.MORPH_CLOSE, kernel)
    elif operation.lower() == "gradient":
        image = cv.morphologyEx(image, cv.MORPH_GRADIENT, kernel)

    return image